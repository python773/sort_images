from tkinter import filedialog
from tkinter import *
import os
import datetime
import sys
from tkinter import ttk
from collections import Counter


print("Function is moving images into folder,\n which named is getting from image_name, \nnp. '001_FC' to folder FC. \nAlso are counting images in folders\nCreated by Marek Szczepkowski 07.10.2019")
def removeEmptyFolders(path, removeRoot=True):
       #'Function to remove empty folders'
    if not os.path.isdir(path):
        return
          # remove empty subfolders
    files = os.listdir(path)
    if len(files):
        for f in files:
            fullpath = os.path.join(path, f)
            if os.path.isdir(fullpath):
                removeEmptyFolders(fullpath)

    # if folder empty, delete it
    files = os.listdir(path)
    if len(files) == 0 and removeRoot:
        os.rmdir(path) 


class Listing:
    def __init__(self,master,st=0):
        self.master=master
        master.title('Sorting images')

        #menu
        self.label_1 = Label(master, text="Path: ")
        self.label_1.grid(row=0)
        self.entry_1=Entry(master, width=40)
        self.entry_1.insert(END,'')
        self.entry_1.grid(row=0,column=1)

        self.label_2 = Label(master, text="")
        self.label_2.grid(row=1, column=1)

        self.button_1 = Button(master, text="Get folder path", command=self.main, height=1,width=15)
        self.button_1.grid(row=0,column=3)

        self.close_button = Button(master, text="Exit", command=root.destroy, height=1,width=5)
        self.close_button.grid(row=3,column=4)

        self.info_button = Button(master, text="Info", command=self.info, height=1,width=5)
        self.info_button.grid(row=3,column=0)        

    def info(self):

        self.label_2['text']="Function is moving images into folder,\n which named is getting from image_name, \nnp. '001_FC' to folder FC. \nAlso are counting images in folders"
        
    def main(self):
        extensions = [".jpg"]
        #get path 
        window=Tk()
        window.withdraw()
        window.title("Sorting images")
        self.label_2['text']="loading files"
        path =  filedialog.askdirectory(parent=window,initialdir=os.getcwd(),title='Please select a directory')
        path=path.replace("/","\\")
        if path=="":
            self.label_2['text']="Select path!"
            return
        self.label_2['text']="Working.."
        self.entry_1.delete(0,END)
        self.entry_1.insert(0,path)        
        file_list = []



        for root, dirs, files in os.walk(path):
            for name in files:
               filename, file_extension = os.path.splitext(name)
               if file_extension in extensions:
                   file_list.append(os.path.join(root, name))

        diction={} #dictionary: camera and images
        for file in file_list:
            camera=file[file.rfind("_")+1:file.rfind(".jpg")] 
            if camera in diction:
                diction[camera].append(file)
            else:
                diction[camera] = [file]

        try:
            new_path=path+"\\"+path[path.rfind("\\")+1:]+"__"+str(len(file_list))+"_images"
            os.mkdir(new_path)
        except OSError:
            print("Folder already exists" % new_path)
            
        for cam in diction:
            len(diction[cam])
            
            try:
                os.mkdir(new_path+"\\"+cam+"_"+str(len(diction[cam]))+"_images")
            except OSError:
                print("Creation of the directory %s failed" % path+"\\"+cam)
            for value in diction[cam]:
                os.rename(value, new_path+"\\"+cam+"_"+str(len(diction[cam]))+"_images"+"\\"+os.path.basename(value))

        #remove_empty_folders
        remove=removeEmptyFolders(path)
        print("Done")
        self.label_2['text']="Done"

        

           
            

        
root=Tk()
bar=Listing(root)
root.mainloop()





